defmodule TaxiBeWeb.Geolocation do
    @geocodingURL "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    @directionsURL "https://api.mapbox.com/directions/v5/mapbox/driving/"
    @token "pk.eyJ1IjoiYTAxNzMwMDY3IiwiYSI6ImNrMXdqcGt2cjAya3YzY28xeDM3NGl3anoifQ.LOGns9RaPNCKuEkJOO36xg" # Replace here your actual access token

    def geocode(address) do
        case HTTPoison.get(
            @geocodingURL <> URI.encode(address) <>
            ".json?access_token=" <> @token
        ) do
            {:ok, %{body: bodyStr}} ->
                { :ok,
                    bodyStr
                    |> Jason.decode!
                    |> Map.fetch!("features")
                    |> Enum.at(0)
                    |> Map.fetch!("center")
                }
            _ -> {:error, "Something wrong with Mapbox call"}
        end
    end

    def distance_and_duration(origin_coord, destination_coord) do
        %{body: body} = HTTPoison.get!(@directionsURL <> "#{Enum.join(origin_coord, ",")};#{Enum.join(destination_coord, ",")}"<> "?access_token=" <> @token)

        %{"duration" => duration, "distance" => distance} =
            body
            |> Jason.decode!
            |> Map.fetch!("routes")
            |> Enum.at(0)

        {distance, duration}
    end

end