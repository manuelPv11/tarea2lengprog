defmodule TaxiBeWeb.BookingController do
    use TaxiBeWeb, :controller
    @geocodingURL "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    @token "pk.eyJ1IjoiYTAxNzMwMDY3IiwiYSI6ImNrMXdqcGt2cjAya3YzY28xeDM3NGl3anoifQ.LOGns9RaPNCKuEkJOO36xg" # Please put here your actual access token

    def create(conn, %{"pickup_address" => pickup_address, "dropoff_address" => dropoff_address}) do
        {:ok, coord1} = TaxiBeWeb.Geolocation.geocode(pickup_address)
        {:ok, coord2} = TaxiBeWeb.Geolocation.geocode(dropoff_address)
        {distance,_} = TaxiBeWeb.Geolocation.distance_and_duration(coord1, coord2)
        IO.inspect "Ride fare: #{Float.ceil(distance/100)}"
        json(conn, %{msg: "We are processing your request"})
    end
end